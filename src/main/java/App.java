import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public class App {
	final static String ARCHIVO_CSV = "contactos.csv";
	final static String OTRO_ARCHIVO_CSV = "mas_contactos.csv";

	public static void main(String[] args) {
		lecturaCsv();
		escrituraCsv();
	}

	
	private static void lecturaCsv() {
		// Forma básica de obtener un CSVReader: delimitador por defecto ',' y lee
		// cabecera
//		CSVReader csv = new CSVReader(new FileReader(archivoCsv));

		// Lectura de todo el fichero de golpe. La cabecera se salta.
		// El delimitador es ';'
		CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
		try (Reader reader = new FileReader(ARCHIVO_CSV);
				CSVReader csv = new CSVReaderBuilder(reader).withCSVParser(parser).withSkipLines(1).build()) {
			List<String[]> contenido = csv.readAll();
			for (String[] linea : contenido) {
				for (String campo : linea) {
					System.out.print(campo + ";");
				}
				System.out.println();
			}
		} catch (IOException | CsvException e) {
			System.out.println("Error: " + e.getMessage());
		}

		System.out.println("---------------------------------------------------");

		// Lectura linea a linea. La cabecera se salta. Delimitador ';'
		try (Reader reader = new FileReader(ARCHIVO_CSV);
				CSVReader csv = new CSVReaderBuilder(reader).withCSVParser(parser).withSkipLines(1).build()) {
			String[] linea = csv.readNext();
			while (linea != null) {
				for (String campo : linea)
					System.out.print(campo + ";");
				System.out.println();
				linea = csv.readNext();
			}
		} catch (IOException | CsvException e) {
			System.out.println("Error: " + e.getMessage());
		}

		System.out.println("---------------------------------------------------");

		// Lectura linea a linea con mapeo manual a clase Contacto
		try (Reader reader = new FileReader(ARCHIVO_CSV);
				CSVReader csv = new CSVReaderBuilder(reader).withCSVParser(parser).withSkipLines(1).build()) {
			String[] linea = csv.readNext();
			linea = csv.readNext();
			Contacto contacto;
			while (linea != null) {
				contacto = new Contacto();
				contacto.setIdentificador(Integer.parseInt(linea[0]));
				contacto.setNomCompanyia(linea[1]);
				contacto.setNomContacto(linea[2]);
				contacto.setDireccion(linea[3]);
				contacto.setCiudad(linea[4]);
				contacto.setPais(linea[5]);
				System.out.println(contacto);
				linea = csv.readNext();
			}
		} catch (IOException | CsvException e) {
			System.out.println("Error: " + e.getMessage());
		}

		System.out.println("---------------------------------------------------");

		// Lectura con mapeo automático
		// Requiere anotaciones en la clase Contacto.
		try {
			List<Contacto> contactos = new CsvToBeanBuilder<Contacto>(new FileReader(ARCHIVO_CSV)).withSeparator(';')
					.withType(Contacto.class).build().parse();
			for (Contacto contacto : contactos)
				System.out.println(contacto);
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}

		// Lectura con mapeo teniendo en cuenta que en el campo nombre_contacto hay
		// varios contactos y queremos separarlos
		// Requiere cambiar la anotación correspondiente en la clase Contacto -> clase
		// Contacto2
		try {
			List<Contacto2> contactos2 = new CsvToBeanBuilder<Contacto2>(new FileReader(ARCHIVO_CSV)).withSeparator(';')
					.withType(Contacto2.class).build().parse();
			for (Contacto2 contacto : contactos2)
				System.out.println(contacto);
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

	private static void escrituraCsv() {

		// Escritura en fichero usando la estructura de la clase contactos2
		List<String> listaNombresContacto = new ArrayList<String>();
		listaNombresContacto.add("persona1");
		listaNombresContacto.add("persona2");
		Contacto2 contacto1 = new Contacto2(1, "Compañia1", listaNombresContacto, "direccion1", "ciudad1", "pais1");
		listaNombresContacto = new ArrayList<String>();
		listaNombresContacto.add("persona3");
		listaNombresContacto.add("persona4");
		Contacto2 contacto2 = new Contacto2(2, "Compañia2", listaNombresContacto, "direccion2", "ciudad2", "pais2");
		List<Contacto2> listaContactos = new ArrayList<Contacto2>();
		listaContactos.add(contacto1);
		listaContactos.add(contacto2);

		try (Writer fw = new FileWriter(OTRO_ARCHIVO_CSV)) {
			StatefulBeanToCsv<Contacto2> csv = new StatefulBeanToCsvBuilder<Contacto2>(fw).withSeparator(';').build();
			csv.write(listaContactos);
		} catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
			System.out.println("Error: " + e.getMessage());
		}

		// Escritura sin una estructura definida por una clase
		String[] lin1 = { "libro", "moneda", "lapiz" };
		String[] lin2 = { "boli", "silla", "lampara" };
		String[] lin3 = { "plato", "mesa", "libro" };

		List<String[]> lineas = new ArrayList<>();
		lineas.add(lin1);
		lineas.add(lin2);
		lineas.add(lin3);

		try (Writer fw = new FileWriter("productos.csv");
				CSVWriter csvWriter = new CSVWriter(fw, ';', CSVWriter.NO_QUOTE_CHARACTER,
						CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
//		try (Writer fw = new FileWriter(OTRO_ARCHIVO_CSV); CSVWriter csvWriter = new CSVWriter(fw)) {			
			String[] cabecera = {"producto1", "producto2", "producto3"};
            csvWriter.writeNext(cabecera);
			csvWriter.writeAll(lineas);
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}
	
}
