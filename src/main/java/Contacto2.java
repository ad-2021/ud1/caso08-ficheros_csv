import java.util.List;

import com.opencsv.bean.CsvBindAndSplitByName;
import com.opencsv.bean.CsvBindByName;

public class Contacto2 {
	@CsvBindByName(column="Identificador")
	private int identificador;
	@CsvBindByName(column="Nombre_Companyia")
	private String nomCompanyia;	
	@CsvBindAndSplitByName(column="Nombre_Contacto", elementType=String.class, splitOn=",+", writeDelimiter=", ")
	private List<String> nomContacto;
	@CsvBindByName(column="Direccion")
	private String direccion;
	@CsvBindByName(column="Ciudad")
	private String ciudad;
	@CsvBindByName(column="Pais")
	private String pais;
	
	public Contacto2() {
		super();
	}
	
	public Contacto2(int identificador, String nomCompanyia, List<String> nomContacto, String direccion, String ciudad, String pais) {
		super();
		this.identificador = identificador;
		this.nomCompanyia = nomCompanyia;
		this.nomContacto = nomContacto;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.pais = pais;
	}

	public int getIdentificador() {
		return identificador;
	}

	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	public String getNomCompanyia() {
		return nomCompanyia;
	}

	public void setNomCompanyia(String nomCompanyia) {
		this.nomCompanyia = nomCompanyia;
	}

	public List<String> getNomContacto() {
		return nomContacto;
	}

	public void setNomContacto(List<String> nomContacto) {
		this.nomContacto = nomContacto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	@Override
	public String toString() {
		return "Contacto [identificador=" + identificador + ", nomCompanyia=" + nomCompanyia + ", nomContacto="
				+ nomContacto.toString() + ", direccion=" + direccion + ", ciudad=" + ciudad + ", pais=" + pais + "]";
	}	
	
	
}
